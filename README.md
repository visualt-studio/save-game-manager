# Save Game Manager

A simple save game manager for Unreal Engine.

## Features

* Callable from almost anywhere within the blueprints!
* Automatically load default save game on game start
* Enqueue your async save/load in sequence
* Callback on save/load

## Installation

### From Source

* Clone or download repository as zip file
* Place extracted folder from the zip in your `<ProjectName>/Plugins`
* Generate UE project files and compile using your preferred IDE

### Binary Files

* Download latest packaged binary files in [releases](https://gitlab.com/visualt-studio/save-game-manager/-/releases) page
* Place extracted folder in your `<ProjectName>/Plugins`
