// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SaveGameObject.generated.h"

/**
 * 
 */
UCLASS()
class SAVEGAMEMANAGER_API UCustomSaveGame : public USaveGame
{
	GENERATED_BODY()
public:
	/** Get save game object creation time. */
	UFUNCTION(BlueprintCallable, Category = Save)
	FDateTime GetTimeCreated() const { return TimeCreated; };

	/** Get save game object modified time. */
	UFUNCTION(BlueprintCallable, Category = Save)
	FDateTime GetTimeModified() const { return TimeModified; };

	void SetTimeCreated(const FDateTime NewTime);
	void SetTimeModified(const FDateTime NewTime);
private:
    FDateTime TimeCreated;
    FDateTime TimeModified;
};
