// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "SaveGameSettings.generated.h"

/**
 * 
 */
UCLASS(Config = Game, defaultconfig, meta = (DisplayName = "Save Game Settings"))
class SAVEGAMEMANAGER_API USaveGameSettings : public UDeveloperSettings
{
	GENERATED_BODY()
public:
	/* Default slot name if UI doesn't specify any */
	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "General")
		FString DefaultSaveSlotName;

	/* Enable load default save game on game start */
	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "General")
		bool bLoadSaveOnStartup;

	USaveGameSettings();
};
