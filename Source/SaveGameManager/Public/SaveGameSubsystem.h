// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "GameFramework/SaveGame.h"
#include "SaveGameSubsystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSaveGameEvent, USaveGame *, SaveGame);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGameSavedEvent, USaveGame *, SaveGame, bool, ReturnValue);

/**
 *
 */
UCLASS()
class SAVEGAMEMANAGER_API USaveGameSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly, Category = "Save")
	FString SaveGameSlotName;

	UPROPERTY(BlueprintAssignable)
	FSaveGameEvent OnSaveGameLoaded;

	UPROPERTY(BlueprintAssignable)
	FGameSavedEvent OnSaveGameSaved;

	/**
	 * Write save game object into storage. Defaults to project save directory.
	 * For Android, it is set to `/storage/emulated/0/data/Android/`.
	 * @param SaveGame Save game object, if not specified, it will use default save game to storage instead
	 * @param SaveName Name of the save file in storage
	 */
	UFUNCTION(BlueprintCallable, Category = "Save", meta = (AdvancedDisplay = "SaveGame, SaveName"))
	bool SaveGameToStorage(USaveGame *SaveGame, const FString SaveName = "SaveGame");

	/**
	 * Load save game object from storage. Defaults to project save directory.
	 * For Android, it is set to `/storage/emulated/0/data/Android/`.
	 * @param SaveGame Save game object
	 * @param SaveName Name of the save file in storage
	 * @return Will return nullptr if not a valid save game object
	 */
	UFUNCTION(BlueprintCallable, Category = "Save", meta = (AdvancedDisplay = 0))
	USaveGame *LoadGameFromStorage(const FString SaveName = "SaveGame");

	/* Get current local save game */
	UFUNCTION(BlueprintCallable, Category = "Save")
	USaveGame* GetSaveGame() const;

	/**
	 * Remove save game with given save name from local storage
	 * @param SaveName Name of the save game to be removed
	 */
	UFUNCTION(BlueprintCallable, Category = "Save")
	bool DeleteSaveGameOnStorage(const FString SaveName = "SaveGame");

	/* Create a save game object based on provided class */
	UFUNCTION(BlueprintCallable, Category = "Save", meta = (DeterminesOutputType = "SaveGameClass", AdvancedDisplay = 1))
	USaveGame *CreateSaveGameObject(TSubclassOf<USaveGame> SaveGameClass, const FString SaveName = "SaveGame");

	/* Check whether save game with given name exists */
	UFUNCTION(BlueprintCallable, Category = "Save")
	bool DoesSaveGameExist(const FString SaveName = "SaveGame") const;

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	bool SaveGameToDisk(USaveGame *SaveGame, const FString SaveName = "SaveGame");

	// Add async action to queue
	void AddActionToQueue(UBlueprintAsyncActionBase *Action);

	// Remove and return oldest async action from queue
	UBlueprintAsyncActionBase *GetActionFromQueue();

	// Get the oldest async action from queue without removing it
	UBlueprintAsyncActionBase *PeekActionFromQueue() const
	{
		return Queue.IsValidIndex(0) ? Queue[0] : nullptr;
	}

	// Check if queue holds any value
	bool IsQueueEmpty() const { return Queue.Num() == 0; }
	void SetSaveGame(USaveGame* SaveGame);

	/* Get save path based on platform */
	FString GetSavePath(const FString FileName) const;

private:
	UPROPERTY(Transient)
	USaveGame *Save;

	UPROPERTY()
	TArray<UBlueprintAsyncActionBase *> Queue;

};

UCLASS()
class SAVEGAMEMANAGER_API UAsyncLoadSaveGameFromStorage : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()
public:
	/**
	 * Asynchoronously load save game from storage into memory
	 * @param SaveName Save game slot name, defaults to the plugin's setting if left empty
	 */
	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", AdvancedDisplay = 1), Category = "Save")
	static UAsyncLoadSaveGameFromStorage *AsyncLoadSaveGameFromStorage(
			USaveGameSubsystem *Subsystem,
			const FString SaveName);

	UPROPERTY(BlueprintAssignable)
	FSaveGameEvent Completed;

	UPROPERTY(BlueprintAssignable)
	FSaveGameEvent Failed;

protected:
	USaveGameSubsystem *SaveSubsystem;
	FString SaveGameSlotName;

	virtual void Activate() override;
	virtual void HandleAsyncLoadGame(
			UAsyncLoadSaveGameFromStorage *Action,
			USaveGameSubsystem *Subsystem,
			const FString SaveName,
			FSaveGameEvent Success,
			FSaveGameEvent Fail);
};

UCLASS()
class SAVEGAMEMANAGER_API UAsyncSaveGameToStorage : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()
public:
	/**
	 * Asynchoronously save game from memory into storage
	 * @param SaveName Save game slot name, defaults to the plugin's setting if left empty
	 */
	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", AdvancedDisplay = "SaveGame, SaveName"), Category = "Save")
	static UAsyncSaveGameToStorage *AsyncSaveGameToStorage(
			USaveGameSubsystem *Subsystem,
			USaveGame *SaveGame,
			const FString SaveName = "SaveGame");

	UPROPERTY(BlueprintAssignable)
	FSaveGameEvent Completed;

	UPROPERTY(BlueprintAssignable)
	FSaveGameEvent Failed;

protected:
	USaveGameSubsystem *SaveSubsystem;
	USaveGame *GameSave;
	FString SaveGameSlotName;

	virtual void Activate() override;
	virtual void HandleAsyncSaveGame(
			UAsyncSaveGameToStorage *Action,
			USaveGameSubsystem *Subsystem,
			USaveGame *SaveGameObject,
			const FString SaveName,
			FSaveGameEvent Success,
			FSaveGameEvent Fail);
};
