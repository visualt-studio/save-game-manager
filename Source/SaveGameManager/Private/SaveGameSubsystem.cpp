// Fill out your copyright notice in the Description page of Project Settings.

#include "SaveGameSubsystem.h"
#include "SaveGameManager.h"
#include "SaveGameSettings.h"
#include "SaveGameObject.h"
#include "Kismet/GameplayStatics.h"

#define ANDROID_DATA TEXT("/storage/emulated/0/Android/data")
#define RUNTIME_SETTING TEXT("/Script/AndroidRuntimeSettings.AndroidRuntimeSettings")

bool USaveGameSubsystem::SaveGameToStorage(USaveGame *SaveGame, const FString SaveName)
{
	const bool SaveResult = SaveGameToDisk(SaveGame, SaveName);
	if (!SaveResult)
	{
		const FString SavePath = GetSavePath(SaveName);
		UE_LOG(LogSaveGame, Warning, TEXT("Failed to save game to %s"), *SavePath);
	}
	OnSaveGameSaved.Broadcast(Save, SaveResult);
	return SaveResult;
}

USaveGame *USaveGameSubsystem::LoadGameFromStorage(const FString SaveName)
{
	TArray<uint8> SaveData;
	FString SavePath = GetSavePath(SaveName);
	if (FPaths::FileExists(SavePath) && FFileHelper::LoadFileToArray(SaveData, *SavePath))
	{
		Save = UGameplayStatics::LoadGameFromMemory(SaveData);
		OnSaveGameLoaded.Broadcast(Save);
		return Save;
	}
	UE_LOG(LogSaveGame, Warning, TEXT("Failed to load game from %s"), *SavePath);
	return nullptr;
}

USaveGame* USaveGameSubsystem::GetSaveGame() const
{
	if (!IsValid(this))	return nullptr;
	return Save;
}

bool USaveGameSubsystem::DeleteSaveGameOnStorage(const FString SaveName)
{
	const FString SaveLocation = GetSavePath(SaveName);

	// if save game doesn't exist, return false
	if (!FPaths::FileExists(SaveLocation))
		return false;

	IFileManager &FileManager = IFileManager::Get();
	return FileManager.Delete(*SaveLocation);
}

USaveGame *USaveGameSubsystem::CreateSaveGameObject(TSubclassOf<USaveGame> SaveGameClass, const FString SaveName)
{
	// Don't save if no class or if class is the abstract base class.
	if (*SaveGameClass && (*SaveGameClass != USaveGame::StaticClass()))
	{
		Save = NewObject<USaveGame>(GetTransientPackage(), SaveGameClass);
		if (Save->StaticClass()->IsChildOf(UCustomSaveGame::StaticClass()))
		{
			UCustomSaveGame* CustomSave = Cast<UCustomSaveGame>(Save);
			if (IsValid(CustomSave))
			{
				CustomSave->SetTimeCreated(FDateTime::UtcNow());
			}
		}
		return Save;
	}
	return nullptr;
}

bool USaveGameSubsystem::DoesSaveGameExist(const FString SaveName) const
{
	const FString SaveLocation = GetSavePath(SaveName);
	return FPaths::FileExists(SaveLocation);
}

void USaveGameSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	const USaveGameSettings* SaveSettings = GetDefault<USaveGameSettings>();
	SaveGameSlotName = SaveSettings->DefaultSaveSlotName;
	const bool Load = SaveSettings->bLoadSaveOnStartup;
	UE_LOG(LogSaveGame, Display, TEXT("EnableLoadAtStartup: %s"), Load ? TEXT("true") : TEXT("false"));

	// Load existing save game on storage to memory
	const FString SaveLocation = GetSavePath(SaveGameSlotName);
	if (Load && FPaths::FileExists(SaveLocation))
	{
		Save = LoadGameFromStorage(SaveGameSlotName);
	}
}

bool USaveGameSubsystem::SaveGameToDisk(USaveGame *SaveGame, const FString SaveName)
{
	// Sync valid save file to internal save game
	if (IsValid(SaveGame))
	{
		Save = SaveGame;
	}

	// Set current time as time modified
	if (Save->StaticClass()->IsChildOf(UCustomSaveGame::StaticClass()))
	{
		UCustomSaveGame* CustomSave = Cast<UCustomSaveGame>(Save);
		if (IsValid(CustomSave))
		{
			CustomSave->SetTimeModified(FDateTime::UtcNow());
		}
	}

	// Set save location to default value if not specified
	FString SaveFileName = SaveName;
	if (SaveFileName.IsEmpty()) SaveFileName = SaveGameSlotName;

	TArray<uint8> SaveData;
	if (UGameplayStatics::SaveGameToMemory(Save, SaveData))
	{
		const FString SaveLocation = GetSavePath(SaveName);
		return FFileHelper::SaveArrayToFile(SaveData, *SaveLocation);
	}
	return false;
}

void USaveGameSubsystem::AddActionToQueue(UBlueprintAsyncActionBase *Action)
{
	Queue.Add(Action);
}

UBlueprintAsyncActionBase *USaveGameSubsystem::GetActionFromQueue()
{
	if (Queue.IsValidIndex(0))
	{
		UBlueprintAsyncActionBase *Action = Queue[0];
		Queue.Remove(Action);
		return Action;
	}
	return nullptr;
}

void USaveGameSubsystem::SetSaveGame(USaveGame* SaveGame)
{
	Save = SaveGame;
}

FString USaveGameSubsystem::GetSavePath(const FString FileName) const
{
	const FString SaveDir = FPaths::Combine(FPaths::ProjectSavedDir(), TEXT("SaveGames"));
	FString SaveLocation = FPaths::Combine(SaveDir, FileName);
#if PLATFORM_ANDROID
	// Set save location to game data directory for scoped access
	FString PackageName;
	GConfig->GetString(RUNTIME_SETTING, TEXT("PackageName"), PackageName, GEngineIni);
	SaveLocation = FPaths::Combine(ANDROID_DATA, PackageName, TEXT("Saved"), FileName);
#endif // PLATFORM_ANDROID
	return SaveLocation;
}

UAsyncLoadSaveGameFromStorage *UAsyncLoadSaveGameFromStorage::AsyncLoadSaveGameFromStorage(
		USaveGameSubsystem *Subsystem,
		const FString SaveName)
{
	if (!IsValid(Subsystem))
	{
		UE_LOG(LogSaveGame, Error, TEXT("Invalid GameSaveManager provided. Is the subsystem initialized properly?"));
		return nullptr;
	}

	UAsyncLoadSaveGameFromStorage *Action = NewObject<UAsyncLoadSaveGameFromStorage>();
	Action->SaveSubsystem = Subsystem;
	Action->SaveGameSlotName = SaveName.IsEmpty() ? Subsystem->SaveGameSlotName : SaveName;
	return Action;
}

void UAsyncLoadSaveGameFromStorage::Activate()
{
	SaveSubsystem->AddActionToQueue(this);
	if (SaveSubsystem->PeekActionFromQueue() == this)
	{
		HandleAsyncLoadGame(this, SaveSubsystem, SaveGameSlotName, Completed, Failed);
	}
}

void UAsyncLoadSaveGameFromStorage::HandleAsyncLoadGame(
		UAsyncLoadSaveGameFromStorage *Action,
		USaveGameSubsystem *Subsystem,
		const FString SaveName,
		FSaveGameEvent Success,
		FSaveGameEvent Fail)
{
	AsyncTask(ENamedThreads::AnyHiPriThreadNormalTask, [Action, Subsystem, SaveName, Success, Fail]()
	{
		// Do the actual I/O on the background thread
		TArray<uint8> ObjectBytes;
		const FString SavePath = Subsystem->GetSavePath(SaveName);
		FFileHelper::LoadFileToArray(ObjectBytes, *SavePath);

		AsyncTask(ENamedThreads::GameThread, [Action, Subsystem, Success, Fail, ObjectBytes, SavePath]()
		{
			USaveGame* LoadedGame = nullptr;
			if (ObjectBytes.Num() > 0)
			{
				LoadedGame = UGameplayStatics::LoadGameFromMemory(ObjectBytes);
				if (IsValid(Subsystem)) Subsystem->SetSaveGame(LoadedGame);
				Success.Broadcast(LoadedGame);
			}
			else {
				UE_LOG(LogSaveGame, Warning, TEXT("Failed to load save game from %s"), *SavePath);
				Fail.Broadcast(LoadedGame);
			}
			// Continue with actions in queue
			if (IsValid(Subsystem))
			{
				Subsystem->OnSaveGameLoaded.Broadcast(LoadedGame);
				Subsystem->GetActionFromQueue();
				if (!Subsystem->IsQueueEmpty())
				{
					// Activate oldest from queue
					Subsystem->PeekActionFromQueue()->Activate();
				}
			}
			Action->SetReadyToDestroy();
		});
	});
}

UAsyncSaveGameToStorage *UAsyncSaveGameToStorage::AsyncSaveGameToStorage(
		USaveGameSubsystem *Subsystem,
		USaveGame *SaveGame,
		const FString SaveName)
{
	if (!IsValid(Subsystem))
	{
		UE_LOG(LogSaveGame, Error, TEXT("Invalid GameSaveManager provided. Is the subsystem initialized properly?"));
		return nullptr;
	}

	UAsyncSaveGameToStorage *Action = NewObject<UAsyncSaveGameToStorage>();
	Action->SaveSubsystem = Subsystem;
	Action->GameSave = SaveGame;
	Action->SaveGameSlotName = SaveName;
	return Action;
}

void UAsyncSaveGameToStorage::Activate()
{
	SaveSubsystem->AddActionToQueue(this);
	if (SaveSubsystem->PeekActionFromQueue() == this)
	{
		HandleAsyncSaveGame(this, SaveSubsystem, GameSave, SaveGameSlotName, Completed, Failed);
	}
}

void UAsyncSaveGameToStorage::HandleAsyncSaveGame(
		UAsyncSaveGameToStorage *Action,
		USaveGameSubsystem *Subsystem,
		USaveGame *SaveGameObject,
		const FString SaveName,
		FSaveGameEvent Success,
		FSaveGameEvent Fail)
{
	AsyncTask(ENamedThreads::AnyHiPriThreadNormalTask, [Action, Subsystem, SaveGameObject, SaveName, Success, Fail]()
	{
		// Do the actual I/O on the background thread
		bool bSuccess = Subsystem->SaveGameToDisk(SaveGameObject, SaveName);

		if (Success.IsBound()) 
		{
			AsyncTask(ENamedThreads::GameThread, [Action, Subsystem, SaveName, Success, Fail, bSuccess]()
			{
				if (bSuccess)
				{
					Success.Broadcast(Subsystem->GetSaveGame());
				}
				else {
					const FString SavePath = Subsystem->GetSavePath(SaveName);
					UE_LOG(LogSaveGame, Warning, TEXT("Failed to save game to %s"), *SavePath);
					Fail.Broadcast(nullptr);
				}
				// Continue with actions in queue
				if (IsValid(Subsystem))
				{
					Subsystem->OnSaveGameSaved.Broadcast(Subsystem->GetSaveGame(), bSuccess);
					Subsystem->GetActionFromQueue();
					if (!Subsystem->IsQueueEmpty())
					{
						// Activate oldest from queue
						Subsystem->PeekActionFromQueue()->Activate();
					}
				}
				Action->SetReadyToDestroy();
			});
		}
	});
}

#undef ANDROID_DATA
#undef RUNTIME_SETTING
