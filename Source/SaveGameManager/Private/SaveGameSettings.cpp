// Fill out your copyright notice in the Description page of Project Settings.


#include "SaveGameSettings.h"

USaveGameSettings::USaveGameSettings()
	: DefaultSaveSlotName(TEXT("SaveGame"))
	, bLoadSaveOnStartup(true)
{
	CategoryName = TEXT("Plugins");
}