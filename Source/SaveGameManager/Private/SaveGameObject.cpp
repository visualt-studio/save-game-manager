#include "SaveGameObject.h"

void UCustomSaveGame::SetTimeCreated(const FDateTime NewTime)
{
	TimeCreated = NewTime;
}

void UCustomSaveGame::SetTimeModified(const FDateTime NewTime)
{
	TimeModified = NewTime;
}
